#read messages from /joy topic and publishes as a /RosAria/cmd_vel topic

import rospy
from geometry_msgs.msg import Twist
from time import sleep
from sensor_msgs.msg import Joy

global re, frente, esquerda, direita
re = 0
frente = 0
esquerda = 0
direita = 0

def joy_callback(data):
    global re
    re = data.buttons[1]
    global frente
    frente = data.buttons[2]
    global esquerda
    esquerda = data.buttons[3]
    global direita
    direita = data.buttons[4]
    return

def exemplo():
    global re, frente, esquerda, direita
    rospy.loginfo("Message received")
    vel = Twist()
    pub_vel = rospy.Publisher("/RosAria/cmd_vel",Twist, queue_size=1)
    rospy.init_node("joy_control")
    rospy.Subscriber("/joy", Joy, joy_callback)
    rate = rospy.Rate(20)
    sleep(0.2)

    while not rospy.is_shutdown():
        vel.linear.x = 0
        vel.linear.y = 0
        vel.linear.z = 0
        vel.angular.x = 0
        vel.angular.y = 0
        vel.angular.z = 0
        if (re == 1):
            vel.linear.x = -0.3
            vel.linear.y = 0
            vel.linear.z = 0
            vel.angular.x = 0
            vel.angular.y = 0
            vel.angular.z = 0
        if (frente == 1):
            vel.linear.x = 0.3
            vel.linear.y = 0
            vel.linear.z = 0
            vel.angular.x = 0
            vel.angular.y = 0
            vel.angular.z = 0
        if (esquerda == 1):
            vel.linear.x = 0
            vel.linear.y = 0
            vel.linear.z = 0
            vel.angular.x = 0
            vel.angular.y = 0
            vel.angular.z = 0.3
        if (direita == 1):
            vel.linear.x = 0
            vel.linear.y = 0
            vel.linear.z = 0
            vel.angular.x = 0
            vel.angular.y = 0
            vel.angular.z = -0.3
        print vel

        pub_vel.publish(vel)
        rate.sleep()


if __name__ == '__main__':
    try:
        exemplo()
    except rospy.ROSInterruptException:
        pass