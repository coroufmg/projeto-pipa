#!/usr/bin/env
import rospy
from geometry_msgs.msg import Twist
from time import sleep
from sensor_msgs.msg import Joy
import rosbag
from nav_msgs.msg import Odometry
import math

def topic_msgs(bagfile, topic):
	msg_array = []
	bag=rosbag.Bag(bagfile)
	for topic, msg, t in bag.read_messages(topics=[topic]): #percorre as mensagens do(s) topico(s) selecionado
		msg_array.append(msg)	#salva as mensagens em uma lista
	return msg_array

path_length = 0
msgs = topic_msgs('volta.bag', '/RosAria/pose')
last_pose = msgs[0]

#calcula distancia total percorrida
for i in range(0, len(msgs)):
	current_pose = msgs[i]
	path_length += math.hypot(current_pose.pose.pose.position.x-last_pose.pose.pose.position.x, current_pose.pose.pose.position.y-last_pose.pose.pose.position.y)
	last_pose = current_pose
print(path_length)