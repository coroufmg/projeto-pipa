clear % Apaga variávels do "Workspace"
close all % Fecha todos os gráficos eventualmente abertos
clc % Limpa janela de comando

addpath('./Functions') % Add "Function" folder
addpath('./BagFiles') % Add "Data" folder

bag = rosbag('./BagFiles/ida.bag');
imu_topic = select(bag, 'Topic', '/imu_data_raw');
msgs = readMessages(imu_topic);

for i = 1:size(msgs,1)
    ang_vel_x(i,1) = msgs{i,1}.AngularVelocity.X;
    ang_vel_y(i,1) = msgs{i,1}.AngularVelocity.Y;
    ang_vel_z(i,1) = msgs{i,1}.AngularVelocity.Z;
    acc_x(i,1) = msgs{i,1}.LinearAcceleration.X;
    acc_y(i,1) = msgs{i,1}.LinearAcceleration.Y;
    acc_z(i,1) = msgs{i,1}.LinearAcceleration.Z;
    %or_x(i,1) = msgs{i,1}.Orientation.X;
    %or_y(i,1) = msgs{i,1}.Orientation.Y;
    %or_z(i,1) = msgs{i,1}.Orientation.Z;
    %or_w(i,1) = msgs{i,1}.Orientation.W;
    timestamp(i,1) = msgs{i,1}.Header.Stamp.Sec + (msgs{i,1}.Header.Stamp.Nsec/1000000000);
end

figure;

subplot(3,1,1)
plot(timestamp, ang_vel_x)
title('Velocidade Angular X')


subplot(3,1,2)
plot(timestamp, ang_vel_y)
title('Velocidade Angular y')


subplot(3,1,3)
plot(timestamp, ang_vel_z)
title('Velocidade Angular Z')

figure;

subplot(3,1,1)
plot(timestamp, acc_x)
title('Aceleracao linear X')

subplot(3,1,2)
plot(timestamp, acc_y)
title('Aceleracao linear Y')

subplot(3,1,3)
plot(timestamp, acc_z)
title('Aceleracao linear Z')

% figure;
% 
% subplot(4,1,1)
% plot(timestamp, or_x)
% title('Orientacao em X')
% 
% subplot(4,1,2)
% plot(timestamp, or_y)
% title('Orientacao em Y')
% 
% subplot(4,1,3)
% plot(timestamp, or_z)
% title('Orientacao em Z')
% 
% subplot(4,1,4)
% plot(timestamp, or_w)
% title('Orientacao em W')